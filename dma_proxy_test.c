/* DMA Proxy Test Application
 *
 * This application is intended to be used with the DMA Proxy device driver. It provides
 * an example application showing how to use the device driver to do user space DMA
 * operations.
 *
 * It has been tested with an AXI DMA system with transmit looped back to receive.
 * The device driver implements a blocking ioctl() function such that a thread is
 * needed for the 2nd channel. Since the AXI DMA transmit is a stream without any
 * buffering it is throttled until the receive channel is running.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include "dma_proxy.h"

struct tx_thread_context {
	int fd;
	struct dma_proxy_channel_interface *ip;	
};

int proxy_create(const char *devname, int non_blocking, struct dma_proxy_channel_interface **ip)
{
	int fd, flags = O_RDWR;

	if (non_blocking)
		flags |= O_NONBLOCK;
	fd = open(devname, flags);

	if (fd < 0) {
		printf("Unable to open %s (error %d)\n", devname, errno);
		return -1;
	}
	
	*ip = (struct dma_proxy_channel_interface *)mmap(NULL, sizeof(struct dma_proxy_channel_interface),
									PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if (*ip == MAP_FAILED) {
		printf("mmap(%s) failed (error %d)\n", devname, errno);
		return -1;
	}

	return fd;
}

void proxy_destroy(const int fd, struct dma_proxy_channel_interface * const ip)
{
	munmap(ip, sizeof(struct dma_proxy_channel_interface));
	close(fd);
}

/* The following function is the transmit thread to allow the transmit and the
 * receive channels to be operating simultaneously. The ioctl calls are blocking
 * such that a thread is needed.
 */
void *tx_thread(void *argp)
{
	int dummy, i;
	struct tx_thread_context *ctx = (struct tx_thread_context *) argp;

	/* Perform the DMA transfer and the check the status after it completes
 	 * as the call blocks til the transfer is done.
 	 */
	i = ioctl(ctx->fd, 0, NULL);
	if (i != 0) {
		if ((errno == EINVAL) && ((ctx->ip->length == 0) || (ctx->ip->count == 0))) {
			/* ioctl() should give EINVAL if transfer length or count are zero */
			printf("Tx ioctl() returned invalid argument as expected\n");
		} else {
			char errmsg[128];
			strerror_r(errno, errmsg, sizeof(errmsg));
			printf("Tx ioctl() returned error %d (%s)\n", errno, errmsg);
			return (void *) -1;
		}
	}

	if (ctx->ip->status != PROXY_NO_ERROR) {
		printf("Proxy tx transfer returned status %d\n", ctx->ip->status);
		return (void *) -1;
	}

	return NULL;
}

/* The following function uses the dma proxy device driver to perform DMA transfers
 * from user space. This app and the driver are tested with a system containing an
 * AXI DMA without scatter gather and with transmit looped back to receive.
 */
int main(int argc, char *argv[])
{
	struct dma_proxy_channel_interface *rx_proxy_interface_p;
	int rx_proxy_fd;
	struct dma_proxy_channel_interface *tx_proxy_interface_p;
	int tx_proxy_fd;
	int i, opt, non_blocking = 0, test_would_block = 0;
	unsigned int count = 1;
	unsigned int test_size = 1048576;
	struct tx_thread_context ctx;

	pthread_t tid;

	printf("DMA proxy test\n");

	while ((opt = getopt(argc, argv, "c:s:nw")) != -1) {
		switch (opt) {
			case 'c':
				/* transfer count */
				count = strtoul(optarg, NULL, 0);
				break;
			case 's':
				/* transfer size */
				test_size = strtoul(optarg, NULL, 0);
				break;
			case 'n':
				/* non-blocking mode */
				non_blocking = 1;
				break;
			case 'w':
				/* in non-blocking mode, test response
				 * to a second ioctl() when one is in progress
				 */
				test_would_block = 1;
				break;
			default:
				printf("Usage: dma_proxy_test [options]\n");
				printf(" -c <count>    transfer count\n");
				printf(" -s <size>     transfer length (bytes)\n");
				printf(" -n            use non-blocking mode\n");
				return -1;
		}
	}

	printf("transfer size: %u count: %u\n", test_size, count);
	printf("%sblocking mode\n", non_blocking ? "non-" : "");
	if (non_blocking && test_would_block)
		printf("ioctl() when ioctl() in progress will be tested\n");

	rx_proxy_fd = proxy_create("/dev/dma_proxy_rx", 0, &rx_proxy_interface_p);
	if (rx_proxy_fd < 0)
		return -1;

	tx_proxy_fd = proxy_create("/dev/dma_proxy_tx", non_blocking, &tx_proxy_interface_p);
	if (tx_proxy_fd < 0)
		return -1;

	/* Set up the length for the DMA transfer and initialize the transmit
 	 * buffer to a known pattern.
 	 */
	tx_proxy_interface_p->length = test_size;
	tx_proxy_interface_p->count = count;

    for (i = 0; i < DMA_PROXY_BUFFER_SIZE; i++)
 		tx_proxy_interface_p->buffer[i] = random();

	/* Initialize the receive buffer so that it can be verified after the transfer is done
	 * and setup the size of the transfer for the receive channel
 	 */
	memset(rx_proxy_interface_p->buffer, 0, DMA_PROXY_BUFFER_SIZE);

    rx_proxy_interface_p->length = test_size;
	rx_proxy_interface_p->count = count;

	/* start DMA Tx */

	if (non_blocking) {
		i = ioctl(tx_proxy_fd, 0, NULL);
		if (i != 0) {
			if ((errno == EINVAL)
			    && ((tx_proxy_interface_p->length == 0) || (tx_proxy_interface_p->count == 0))) {
				/* ioctl() should give EINVAL if transfer length or count are zero */
				printf("Tx ioctl() returned invalid argument as expected\n");
			} else {
				char errmsg[128];
				strerror_r(errno, errmsg, sizeof(errmsg));
				printf("Tx ioctl() returned error %d (%s)\n", errno, errmsg);
				return -1;
			}
		}

		if (test_would_block) {
			/* try calling ioctl() again - it should be rejected */
			i = ioctl(tx_proxy_fd, 0, NULL);
			if (i != 0) {
				if (errno == EAGAIN) {
					printf("Second Tx ioctl() was correctly rejected\n");
				} else {
					printf("Second Tx ioctl() failed with error %d\n", errno);
					return -1;
				}
			} else {
				printf("Second Tx ioctl() was ACCEPTED!\n");
				return -1;
			}		
		}

	} else {
		ctx.fd = tx_proxy_fd;
		ctx.ip = tx_proxy_interface_p;
		pthread_create(&tid, NULL, tx_thread, &ctx);
		sleep(1);
	}

	/* start DMA Rx */

	i = ioctl(rx_proxy_fd, 0, NULL);
	if (i != 0) {
		if ((errno == EINVAL)
		    && ((rx_proxy_interface_p->length == 0) || (rx_proxy_interface_p->count == 0))) {
			/* ioctl() should give EINVAL if transfer length or count are zero */
			printf("Rx ioctl() returned invalid argument as expected\n");
		} else {
			char errmsg[128];
			strerror_r(errno, errmsg, sizeof(errmsg));
			printf("Rx ioctl() returned error %d (%s)\n", errno, errmsg);
			return -1;
		}
	}


	if (rx_proxy_interface_p->status != PROXY_NO_ERROR) {
		printf("Proxy rx transfer returned status: %d\n", rx_proxy_interface_p->status);
		return -1;
	}

	if (non_blocking) {
		if (tx_proxy_interface_p->status != PROXY_NO_ERROR) {
			printf("Proxy tx transfer returned status: %d\n", tx_proxy_interface_p->status);
			return -1;
		}
	} else {
		void *ret;
		i = pthread_join(tid, &ret);
		if (i != 0) {
			printf("pthread_join() returned error %d\n", i);
			return -1;
		}
		if (ret != NULL) {
			printf("Tx thread returned error status\n");
			return -1;
		}
	}

	/* Verify the data recieved matchs what was sent (tx is looped back to tx)
 	 */
	for (i = 0; i < test_size * count; i++) {
        	if (tx_proxy_interface_p->buffer[i] !=
            		rx_proxy_interface_p->buffer[i]) {
            		printf("buffer not equal, index = %d\n", i);
					return -1;
			}
    	}

	/* Unmap the proxy channel interface memory and close the device files before leaving
	 */

	proxy_destroy(tx_proxy_fd, tx_proxy_interface_p);
	proxy_destroy(rx_proxy_fd, rx_proxy_interface_p);	

	printf("+++ TEST PASSED +++\n");

	return 0;
}
