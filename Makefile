obj-${CONFIG_DMA_PROXY} += dma_proxy.o

# This module may be built outside the kernel tree with, for example:
#
# CONFIG_DMA_PROXY=m make -C <KDIR> O=<BUILD_DIR> M=`readlink -f .` modules
#
# where KDIR is the kernel source root, and BUILD_DIR is the kernel build
# directory, if a relative path this must be relative to the kernel source root.
