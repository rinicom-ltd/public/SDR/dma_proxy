/* DMA Proxy
 *
 * This module is designed to be a small example of a DMA device driver that is
 * a client to the DMA Engine using the AXI DMA driver. It serves as a proxy for
 * kernel space DMA control to a user space application.
 *
 * A zero copy scheme is provided by allowing user space to mmap a kernel allocated
 * memory region into user space, referred to as a proxy channel interface. The
 * ioctl function is provided to start a DMA transfer which then blocks until the
 * transfer is complete. No input arguments are being used in the ioctl function.
 *
 * There is an associated user space application, dma_proxy_test.c, and dma_proxy.h
 * that work with this device driver.
 *
 * The hardware design was tested with an AXI DMA without scatter gather and
 * with the transmit channel looped back to the receive channel.
 *
 */

#include <linux/dmaengine.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/dma-mapping.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/workqueue.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include "dma_proxy.h"

#define CACHED_BUFFERS
//#define INTERNAL_TEST

/* The following module parameter controls where the allocated interface memory area is cached or not
 * such that both can be illustrated.  Add cached_buffers=1 to the command line insert of the module
 * to cause the allocated memory to be cached.
 */
static unsigned cached_buffers = 0;
module_param(cached_buffers, int, S_IRUGO);

MODULE_LICENSE("GPL");

#define DRIVER_NAME 		"dma_proxy"
#define CHANNEL_COUNT 		2
#define ERROR 			-1

static struct class *dma_proxy_class; /* sysfs class */

/* The DMA proxy device
 */

struct dma_proxy_device {
	struct list_head node;
	struct platform_device *pdev; /* associated platform device */
	struct list_head chans;	
};


/* The following data structure represents a single channel of DMA, transmit or receive in the case
 * when using AXI DMA.  It contains all the data to be maintained for the channel.
 */
struct dma_proxy_channel {
	struct dma_proxy_channel_interface *interface_p;	/* user to kernel space interface */
	dma_addr_t interface_phys_addr;

	struct device *proxy_device_p;				/* character device support */
	struct device *dma_device_p;
	dev_t dev_node;
	struct cdev cdev;

	struct dma_chan *channel_p;				/* dma support */
	struct completion cmp;
	dma_cookie_t cookie;
	dma_addr_t dma_handle;
	u32 direction;						/* DMA_MEM_TO_DEV or DMA_DEV_TO_MEM */
	unsigned int desc_count; /* count of completed descriptors */
	struct platform_device *pdev;
	struct list_head node;
	spinlock_t lock;
};

static void sync_callback(void *param);

static dma_cookie_t prep_and_submit(struct dma_proxy_channel *pchannel_p)
{
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	struct dma_async_tx_descriptor *chan_desc;
	dma_cookie_t cookie;

	struct dma_proxy_channel_interface *interface_p = pchannel_p->interface_p;

	/* Create a buffer (channel)  descriptor for the buffer since only a
	 * single buffer is being used for this transfer
	 */

	dma_addr_t addr = pchannel_p->dma_handle
		+ pchannel_p->desc_count * interface_p->length;
	chan_desc = dmaengine_prep_slave_single(pchannel_p->channel_p,
											addr,
											interface_p->length,
											pchannel_p->direction,
											flags);

	/* Make sure the operation was completed successfully
	 */
	if (!chan_desc) {
		printk(KERN_ERR "dmaengine_prep_slave_single error\n");
		cookie = -EBUSY;
	} else {
		chan_desc->callback = sync_callback;
		chan_desc->callback_param = pchannel_p;
		pchannel_p->interface_p->status = PROXY_BUSY;

		cookie = dmaengine_submit(chan_desc);
		if (dma_submit_error(cookie)) {
			dev_info(&pchannel_p->pdev->dev, "dmaengine_submit failed: %d", cookie);
			pchannel_p->interface_p->status = PROXY_ERROR;
			return -EBUSY;
		}

		/* Start the DMA transaction which was previously queued up in the DMA engine
		 */
		dma_async_issue_pending(pchannel_p->channel_p);

	}

	return cookie;	
}

/* Handle a callback and indicate the DMA transfer is complete to another
 * thread of control
 */
static void sync_callback(void *param)
{
	struct dma_proxy_channel *pchannel_p = (struct dma_proxy_channel *)param;
	enum dma_status status;
	unsigned long flags;

	spin_lock_irqsave(&pchannel_p->lock, flags);

	status = dma_async_is_tx_complete(pchannel_p->channel_p, pchannel_p->cookie, NULL, NULL);
	
	if (status == DMA_COMPLETE) {
		pchannel_p->desc_count++;
		if (pchannel_p->desc_count == pchannel_p->interface_p->count) {
			pchannel_p->desc_count = 0;
		}
		pchannel_p->interface_p->index = pchannel_p->desc_count;
		
		if (pchannel_p->desc_count == 0 && !(pchannel_p->interface_p->flags & DMA_PROXY_CYCLIC)) {
			/* all done */
			pchannel_p->interface_p->status = PROXY_NO_ERROR;
		} else {
			/* submit next transaction */
			pchannel_p->cookie = prep_and_submit(pchannel_p);
			spin_unlock_irqrestore(&pchannel_p->lock, flags);
			return;
		}
	} else {
		/* any other status is unexpected */
		dev_err(&pchannel_p->pdev->dev,
			"chan %p dma_async_is_tx_complete() returned %d desc_count %d",
			pchannel_p->channel_p,
			status,
			pchannel_p->desc_count);
		pchannel_p->interface_p->status = PROXY_ERROR;
	}

	if (cached_buffers) {

		/* Cached buffers need to be unmapped after the transfer is done so that the CPU
		 * can see the new data when being received.
		 */
		u32 map_direction;
		if (pchannel_p->direction == DMA_MEM_TO_DEV)
			map_direction = DMA_TO_DEVICE;
		else
			map_direction = DMA_FROM_DEVICE;

		dma_unmap_single(pchannel_p->dma_device_p, pchannel_p->dma_handle,
						 pchannel_p->interface_p->length * pchannel_p->interface_p->count,
						 map_direction);
	}

	complete(&pchannel_p->cmp);
	spin_unlock_irqrestore(&pchannel_p->lock, flags);

}

/* Prepare a DMA buffer to be used in a DMA transaction, submit it to the DMA engine
 * to queued and return a cookie that can be used to track that status of the
 * transaction
 */
static dma_cookie_t start_transfer(struct dma_proxy_channel *pchannel_p)
{
	pchannel_p->desc_count = 0;
	pchannel_p->interface_p->index = 0;
	/* Initialize the completion for the transfer and before using it
	 * then submit the transaction to the DMA engine so that it's queued
	 * up to be processed later and get a cookie to track it's status
	 */
	reinit_completion(&pchannel_p->cmp);

	return prep_and_submit(pchannel_p);
}

/* Wait for a DMA transfer that was previously submitted to the DMA engine
 * wait for it complete, timeout or have an error
 */
static int wait_for_transfer(struct dma_proxy_channel *pchannel_p)
{
	unsigned long timeout;
	int ret;

	/* Wait for the transaction to complete, or timeout, or get an error
	 */
	if (pchannel_p->interface_p->timeout_ms != 0) {
		timeout = msecs_to_jiffies(pchannel_p->interface_p->timeout_ms);
		timeout = wait_for_completion_timeout(&pchannel_p->cmp, timeout);

		if (timeout == 0)  {
			pchannel_p->interface_p->status  = PROXY_TIMEOUT;
			dev_err(&pchannel_p->pdev->dev, "chan %p: DMA timed out\n", pchannel_p->channel_p);
		}

		return 0;

	} else {
		ret = wait_for_completion_interruptible(&pchannel_p->cmp);
		if (ret == -ERESTARTSYS)
			dev_err(&pchannel_p->pdev->dev, "chan %p: received signal while waiting\n", pchannel_p->channel_p);
		return ret;
	}
	
}

/* For debug only, print out the channel details
 */
void print_channel(struct dma_proxy_channel *pchannel_p)
{
	struct dma_proxy_channel_interface *interface_p = pchannel_p->interface_p;

	printk("length = %d ", interface_p->length);
	if (pchannel_p->direction == DMA_MEM_TO_DEV)
		printk("tx direction ");
	else
		printk("rx direction ");
}

/* Setup the DMA transfer for the channel by taking care of any cache operations
 * and the start it.
 */
static int transfer(struct dma_proxy_channel *pchannel_p, unsigned int nonblock)
{
	struct dma_proxy_channel_interface *interface_p = pchannel_p->interface_p;
	u32 map_direction;
	unsigned long flags;

	spin_lock_irqsave(&pchannel_p->lock, flags);

	//print_channel(pchannel_p);

	if (cached_buffers) {

		/* Cached buffers need to be handled before starting the transfer so that
		 * any cached data is pushed to memory.
		 */
		if (pchannel_p->direction == DMA_MEM_TO_DEV)
			map_direction = DMA_TO_DEVICE;
		else
			map_direction = DMA_FROM_DEVICE;
		pchannel_p->dma_handle = dma_map_single(pchannel_p->dma_device_p,
							interface_p->buffer,
							interface_p->length * interface_p->count,
							map_direction);
	} else {

		/* The physical address of the buffer in the interface is needed for the dma transfer
		 * as the buffer may not be the first data in the interface
		 */
		u32 offset = (u32)&interface_p->buffer - (u32)interface_p;
		pchannel_p->dma_handle = (dma_addr_t)(pchannel_p->interface_phys_addr + offset);
	}

	/* Start the DMA transfer and make sure there were not any errors
	 */
	//printk(KERN_INFO "Starting DMA transfers\n");
	pchannel_p->cookie = start_transfer(pchannel_p);

	spin_unlock_irqrestore(&pchannel_p->lock, flags);

	if (dma_submit_error(pchannel_p->cookie)) {
		printk(KERN_ERR "xdma_prep_buffer error\n");
		return -EBUSY;
	}

	if (!nonblock)
		return wait_for_transfer(pchannel_p);
	else
		return 0;
}

/* The following functions are designed to test the driver from within the device
 * driver without any user space.
 */
#ifdef INTERNAL_TEST
static void tx_test(struct work_struct *unused)
{
	transfer(&channels[0]);
}
static void test(void)
{
	int i;
	const int test_size = 8192;
	struct work_struct work;

	/* Initialize the transmit buffer with a pattern and then start
	 * the seperate thread of control to handle the transmit transfer
	 * since the functions block waiting for the transfer to complete.
	 */
	for (i = 0; i < test_size; i++)
		channels[0].interface_p->buffer[i] = i;

	channels[0].interface_p->length = test_size;

	INIT_WORK(&work, tx_test);
	schedule_work(&work);

	/* Initialize the receive buffer with zeroes so that we can be sure
	 * the transfer worked, then start the receive transfer.
	 */
	for (i = 0; i < test_size; i++)
		channels[1].interface_p->buffer[i] = 0;

	channels[1].interface_p->length = test_size;
	transfer(&channels[1]);

	/* Verify the receiver buffer matches the transmit buffer to
	 * verify the transfer was good
	 */
	for (i = 0; i < test_size; i++)
		if (channels[0].interface_p->buffer[i] !=
			channels[1].interface_p->buffer[i]) {
			printk("buffer not equal, index = %d\n", i);
			break;
		}
}
#endif

/* Map the memory for the channel interface into user space such that user space can
 * access it taking into account if the memory is not cached.
 */
static int mmap(struct file *file_p, struct vm_area_struct *vma)
{
	struct dma_proxy_channel *pchannel_p = (struct dma_proxy_channel *)file_p->private_data;

	/* The virtual address to map into is good, but the page frame will not be good since
	 * user space passes a physical address of 0, so get the physical address of the buffer
	 * that was allocated and convert to a page frame number.
	 */
	if (cached_buffers) {
		if (remap_pfn_range(vma, vma->vm_start,
				virt_to_phys((void *)pchannel_p->interface_p)>>PAGE_SHIFT,
				vma->vm_end - vma->vm_start, vma->vm_page_prot))
			return -EAGAIN;
		return 0;
	} else

		/* Step 3, use the DMA utility to map the DMA memory into space so that the
		 * user space application can use it
		 */
		return dma_mmap_coherent(pchannel_p->dma_device_p, vma,
					pchannel_p->interface_p, pchannel_p->interface_phys_addr,
					vma->vm_end - vma->vm_start);
}

/* Open the device file and set up the data pointer to the proxy channel data for the
 * proxy channel such that the ioctl function can access the data structure later.
 */
static int local_open(struct inode *ino, struct file *file)
{
	file->private_data = container_of(ino->i_cdev, struct dma_proxy_channel, cdev);

	return 0;
}

/* Close the file and there's nothing to do for it
 */
static int release(struct inode *ino, struct file *file)
{
	return 0;
}

/* Perform I/O control to start a DMA transfer.
 */
static long ioctl(struct file *file, unsigned int unused , unsigned long arg)
{
	struct dma_proxy_channel *pchannel_p = (struct dma_proxy_channel *)file->private_data;
	unsigned long total_size = pchannel_p->interface_p->length * pchannel_p->interface_p->count;
	int ret;

	dev_dbg(pchannel_p->dma_device_p, "ioctl\n");

	if ((total_size == 0) || (total_size > DMA_PROXY_BUFFER_SIZE)) {
		return -EINVAL;
	}

	if (file->f_flags & O_NONBLOCK) {
		/* check if a transaction is in progress */
		
		if ((pchannel_p->cookie >= DMA_MIN_COOKIE)
			&& (dma_async_is_tx_complete(pchannel_p->channel_p, pchannel_p->cookie, NULL, NULL) != DMA_COMPLETE)) {
				return -EAGAIN;
			}
	}

	/* Step 2, call the transfer function for the channel to start the DMA and wait
	 * for it to finish (blocking in the function).
	 */
	ret = transfer(pchannel_p, file->f_flags & O_NONBLOCK);

	return ret;
}

static struct file_operations dm_fops = {
	.owner    = THIS_MODULE,
	.open     = local_open,
	.release  = release,
	.unlocked_ioctl = ioctl,
	.mmap	= mmap
};


/* Initialize the driver to be a character device such that is responds to
 * file operations.
 */
static int cdevice_init(struct dma_proxy_channel *pchannel_p, const char *name)
{
	int rc;
	char device_name[] = "dma_proxy_%s";

	/* Allocate a character device from the kernel for this
	 * driver
	 */
	rc = alloc_chrdev_region(&pchannel_p->dev_node, 0, 1, DRIVER_NAME);

	if (rc) {
		dev_err(pchannel_p->dma_device_p, "unable to get a char device number\n");
		return rc;
	}

	/* Initialize the ter device data structure before
	 * registering the character device with the kernel
	 */
	cdev_init(&pchannel_p->cdev, &dm_fops);
	pchannel_p->cdev.owner = THIS_MODULE;
	rc = cdev_add(&pchannel_p->cdev, pchannel_p->dev_node, 1);

	if (rc) {
		dev_err(pchannel_p->dma_device_p, "unable to add char device\n");
		goto init_error1;
	}


	/* Create the device node in /dev so the device is accessible
	 * as a character device
	 */
	pchannel_p->proxy_device_p = device_create(dma_proxy_class,
					&pchannel_p->pdev->dev,
					pchannel_p->dev_node,
					NULL, device_name, name);

	if (IS_ERR(pchannel_p->proxy_device_p)) {
		dev_err(pchannel_p->dma_device_p, "unable to create the device\n");
		goto init_error2;
	}

	return 0;

init_error2:
	cdev_del(&pchannel_p->cdev);

init_error1:
	unregister_chrdev_region(pchannel_p->dev_node, 1);
	return rc;
}

/* Exit the character device by freeing up the resources that it created and
 * disconnecting itself from the kernel.
 */
static void cdevice_exit(struct dma_proxy_channel *pchannel_p)
{
	/* Take everything down in the reverse order
	 * from how it was created for the char device
	 */
	if (pchannel_p->proxy_device_p) {
		device_destroy(dma_proxy_class, pchannel_p->dev_node);

		cdev_del(&pchannel_p->cdev);
		unregister_chrdev_region(pchannel_p->dev_node, 1);
	}
}


/* Create a DMA channel by getting a DMA channel from the DMA Engine and then setting
 * up the channel as a character device to allow user space control.
 */
static int create_channel(struct dma_proxy_channel *pchannel_p,
			enum dma_transfer_direction direction,
			const char *chan_name)
{
	int rc;
	struct device *dev = &pchannel_p->pdev->dev;

	/* Request the DMA channel from the DMA engine and then use the device from
	 * the channel for the proxy channel also.
	 */

	pchannel_p->channel_p = dma_request_slave_channel(
				&pchannel_p->pdev->dev, chan_name);
	if (!pchannel_p->channel_p) {
		pr_err("Unable to request DMA channel %s\n", chan_name);
		return -EPROBE_DEFER;
	}
	pchannel_p->dma_device_p = &pchannel_p->channel_p->dev->device;


	dev_info(dev, "channel %s is 0x%px\n",chan_name,	pchannel_p->channel_p);

	/* Initialize the character device for the dma proxy channel
	 */

	rc = cdevice_init(pchannel_p, chan_name);
	if (rc) {
		return rc;
	}

	pchannel_p->direction = direction;

	/* Allocate memory for the proxy channel interface for the channel as either
	 * cached or non-cache depending on input parameter. Use the managed
	 * device memory when possible but right now there's a bug that's not understood
	 * when using devm_kzalloc rather than kzalloc, so stay with kzalloc.
	 */
	if (cached_buffers) {
		pchannel_p->interface_p = (struct dma_proxy_channel_interface *)
			kzalloc(sizeof(struct dma_proxy_channel_interface),
					GFP_KERNEL);
		dev_info(dev, "Allocating cached memory at 0x%08X\n",
			   (unsigned int)pchannel_p->interface_p);
	} else {

		/* Step 1, set dma memory allocation for the channel so that all of memory
		 * is available for allocation, and then allocate the uncached memory (DMA)
		 * for the channel interface
		 */
		dma_set_coherent_mask(pchannel_p->proxy_device_p, 0xFFFFFFFF);
		pchannel_p->interface_p = (struct dma_proxy_channel_interface *)
			dmam_alloc_coherent(pchannel_p->proxy_device_p,
					sizeof(struct dma_proxy_channel_interface),
					&pchannel_p->interface_phys_addr, GFP_KERNEL);
		dev_info(dev, "Allocating uncached memory at 0x%08X\n",
			   (unsigned int)pchannel_p->interface_p);
	}
	if (!pchannel_p->interface_p) {
		dev_err(dev, "DMA allocation error\n");
		return -ENOMEM;
	}

	/* Default timeout */
	pchannel_p->interface_p->timeout_ms = DEFAULT_TIMEOUT;

	/* Initialise the completion object */
	
	init_completion(&pchannel_p->cmp);

	return 0;
}

/* Take care of the char device infrastructure for each
 * channel
 */

/* Take care of the DMA channels and the any buffers allocated
 * for the DMA transfers. The DMA buffers are using managed
 * memory such that it's automatically done.
 */

static int destroy_channel(struct dma_proxy_channel *chan)
{

	cdevice_exit(chan);

	if (chan->channel_p)
		dma_release_channel(chan->channel_p);

	if (chan->interface_p && cached_buffers)
		kfree((void *)chan->interface_p);	

	return 0;
}


static int dma_proxy_probe(struct platform_device *pdev)
{
	int rc = 0;
	struct device_node *of_node = pdev->dev.of_node;
	struct dma_proxy_device *dpdev;
	int i, nchans;
	const char *name, *dirstr;
	enum dma_transfer_direction dir;
	struct dma_proxy_channel *chan;

	dev_info(&pdev->dev, "%s called\n", __func__);

	nchans = of_property_count_strings(of_node, "dma-names");
	if (nchans == 0) {
		dev_err(&pdev->dev, "No channels specified\n");
		return -EINVAL;
	} else if (nchans < 0) {
		dev_err(&pdev->dev, "Error reading dma-names from DT: %d\n",
			nchans);
		return nchans;
	}


	dpdev = devm_kzalloc(&pdev->dev, sizeof(*dpdev), GFP_KERNEL);
	if (dpdev == NULL)
		return -ENOMEM;

	dpdev->pdev = pdev;
	INIT_LIST_HEAD(&dpdev->chans);

	for (i = 0; i < nchans; i++) {
		rc = of_property_read_string_index(of_node, "dma-names",
						i, &name);
		if (rc != 0)
			goto remove_chans;

		rc = of_property_read_string_index(of_node, "dma-dirs",
						i, &dirstr);
		if (rc != 0) {
			dev_err(&pdev->dev, "Error reading dma-dirs from DT: %d", rc);
			goto remove_chans;
		}

		if (strcmp(dirstr, "mem_to_dev") == 0)
			dir = DMA_MEM_TO_DEV;
		else if (strcmp(dirstr, "dev_to_mem") == 0)
			dir = DMA_DEV_TO_MEM;
		else {
			dev_err(&pdev->dev, "DMA direction not recognised: %s", dirstr);
			goto remove_chans;
		}

		chan = devm_kzalloc(&pdev->dev, sizeof(*chan), GFP_KERNEL);
		if (chan == NULL) {
			rc = -ENOMEM;
			goto remove_chans;
		}

		chan->pdev = pdev;
		rc = create_channel(chan, dir, name);

		if (rc)
			goto remove_chans;

		spin_lock_init(&chan->lock);

		list_add(&chan->node, &dpdev->chans);

	}

#ifdef INTERNAL_TEST
	test();
#endif

	platform_set_drvdata(pdev, dpdev);

	return 0;

remove_chans:

	list_for_each_entry(chan, &dpdev->chans, node)
		destroy_channel(chan);

	return rc;

}

static int dma_proxy_remove(struct platform_device *pdev)
{
	struct dma_proxy_device *dpdev = NULL;
	struct dma_proxy_channel *chan;

	dev_info(&pdev->dev, "%s called\n", __func__);

	dpdev = (struct dma_proxy_device *)platform_get_drvdata(pdev);

	list_for_each_entry(chan, &dpdev->chans, node)
		destroy_channel(chan);

	dev_info(&pdev->dev, "%s complete\n", __func__);

	return 0;
}

static const struct of_device_id dma_proxy_of_ids[] = {
	{ .compatible = "rinicom,dma-proxy-1.00"},
	{}
};

static struct platform_driver dma_proxy_driver = {
	.driver = {
		.name = "rinicom_dma_proxy",
		.owner = THIS_MODULE,
		.of_match_table = dma_proxy_of_ids,
	},
	.probe = dma_proxy_probe,
	.remove = dma_proxy_remove,
};

/* Initialize the dma proxy device driver module.
 */
static int __init dma_proxy_init(void)
{
	printk(KERN_INFO "%s module init\n", THIS_MODULE->name);

	/* Only one class in sysfs is to be created for multiple channels,
	 * create the device in sysfs which will allow the device node
	 * in /dev to be created
	 */

	dma_proxy_class = class_create(THIS_MODULE, DRIVER_NAME);

	if (IS_ERR(dma_proxy_class)) {
		pr_err("%s: unable to create class\n", THIS_MODULE->name);
		return (int)dma_proxy_class;
	}

	return platform_driver_register(&dma_proxy_driver);
}

/* Exit the dma proxy device driver module.
 */
static void __exit dma_proxy_exit(void)
{
	printk(KERN_INFO "%s module exiting\n", THIS_MODULE->name);

	platform_driver_unregister(&dma_proxy_driver);

	class_destroy(dma_proxy_class);
}

module_init(dma_proxy_init);
module_exit(dma_proxy_exit);
MODULE_LICENSE("GPL");
