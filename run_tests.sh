#!/bin/bash

# Run a series of tests on the dma_proxy module
# This requires the system to be using the axi_dma_test design

TESTBIN=./dma_proxy_test

# Default single transfer in blocking mode
$TESTBIN || { echo "FAILED!" && exit -1; }

# Multiple transfers in blocking mode
$TESTBIN -s 65536 -c 8 || { echo "FAILED!" && exit -1; }

# Multiple transfers in non-blocking mode
$TESTBIN -s 131072 -c 3 -n || { echo "FAILED!" && exit -1; }

# --- abnormal cases ---

# Zero-size transfer
$TESTBIN -s 0 -c 10 || { echo "FAILED!" && exit -1; }

# Zero-count transfer
$TESTBIN -c 0 || { echo "FAILED!" && exit -1; }

# Calling ioctl() when already in progress
$TESTBIN -s 131072 -c 3 -n -w || { echo "FAILED!" && exit -1; }

echo "=== ALL TESTS PASSED ==="
